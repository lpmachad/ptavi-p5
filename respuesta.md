# Ejercicio 3. Análisis general

¿Cuántos paquetes componen la captura?
## 1050 paquetes componen la captura
¿Cuánto tiempo dura la captura?
## 10,52 segundos dura la captura
¿Qué IP tiene la máquina donde se ha efectuado la captura?
## La IP es 192.168.1.116
¿Se trata de una IP pública o de una IP privada?
## Es una IP privada
¿Por qué lo sabes?
## Lo sabemos porque la IP comienza por 192 y eso nos indica de que tipo es

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
## Los tres protocolos principales son: SIP, UDP y RTP
¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
## Los otros protocolos que podemos ver en la jerarquía son ICMP y IPv4
¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
## El protocolo es RTP con 39.25 bytes

Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo corresponde con una llamada SIP.

Filtra por sip para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

¿En qué segundos tienen lugar los dos primeros envíos SIP?
## Desde el primer instante (0.000012938) y luego en el 0.063305950
Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
## Se empiezan a enviar en la trama numero 6
Los paquetes RTP, ¿cada cuánto se envían?
## Se envían cada 0.01 segundos

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 4. Primeras tramas
Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

¿De qué protocolo de nivel de aplicación son?
## Se trata del protocolo SIP
¿Cuál es la dirección IP de la máquina "Linphone"?
## La dirección IP es 192.168.1.116
¿Cuál es la dirección IP de la máquina "Servidor"?
## La dirección IP de la máquina "Servidor" es 212.79.111.155
¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
## Se realiza una petición request a music@sio.iptel.org 
¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
## Le envía un mensaje de tipo trying 100, indicando que se va a intentar la conexión

Ahora, veamos las dos tramas siguientes.

¿De qué protocolo de nivel de aplicación son?
## El protocolo es SIP
¿Entre qué máquinas se envía cada trama?
## Entre servidor y el cliente (Linphone) y luego entre Linphone y servidor
¿Qué ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
## Le envía un mensaje de 200 OK indicando que se ha realizado correctamente la petición
¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
## Se envía un ACK al servidor indicando que ha llegado la confirmación para establecer la conexión

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 5. Tramas finales
Después de la trama 250, busca la primera trama SIP.

¿Qué número de trama es?
## La trama es la 1042
¿De qué máquina a qué máquina va?
## La trama va dirigida al servidor desde el cliente (Linphone)
¿Para qué sirve?
## La trama se trata de un mensaje de BYE, que indica que la conexión se cierra
¿Puedes localizar en ella qué versión de Linphone se está usando?
## La version la encontramos en User-Agent: Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 6. Invitación
Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

¿Cuál es la dirección SIP con la que se quiere establecer una llamada?
## La dirección SIP es music@ip.iptel.org
¿Qué instrucciones SIP entiende el UA?
## Las instrucciones SIP que entiende son: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE
¿Qué cabecera SIP indica que la información de sesión va en formato SDP?
## La cabeza que indica la información de sesión es Content-Type: application/sdp
¿Cuál es el nombre de la sesión SIP?
## Session Name (s): Talk

# Ejercicio 7. Indicación de comienzo de conversación
En la propuesta SDP de Linphone puede verse un campo m con un valor que empieza por audio 7078.

¿Qué trama lleva esta propuesta?
## La primera trama
¿Qué indica el 7078?
## Indica el puerto por donde van a entrar los datos
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## Es el puerto de destino de los paquetes
¿Qué paquetes son esos?
## Paquetes RTP

En la respuesta a esta propuesta vemos un campo m con un valor que empieza por audio XXX.

¿Qué trama lleva esta respuesta?
## La cuarta trama
¿Qué valor es el XXX?
## 29448
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## Es el puerto por el que se enviaran/recibirán las tramas
¿Qué paquetes son esos?
## Paquetes RTP

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 8. Primeros paquetes RTP
Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

¿De qué máquina a qué máquina va?
## Del cliente al servidor
¿Qué tipo de datos transporta?
## Audio (datos multimedia codificados)
¿Qué tamaño tiene?
## 214 bytes
¿Cuántos bits van en la "carga de pago" (payload)
## 1280 bits (160 bytes)
¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
## 0.02 segundos

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 9. Flujos RTP
Vamos a ver más a fondo el intercambio RTP. Busca en el menú Telephony la opción RTP. Empecemos mirando los flujos RTP.

¿Cuántos flujos hay? ¿Por qué?
## Tenemos dos flujos, del cliente al servidor y del servidor al cliente, porque se trata de una llamada
¿Cuántos paquetes se pierden?
## No se pierde ningún paquete (en Lost = 0 se puede observar)
Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
## El valor máximo es 30.726700 ms
¿Qué es lo que significa el valor de delta?
## La latencia, suma de los retardos
¿En qué flujo son mayores los valores de jitter (medio y máximo)?
## En el segundo flujo, cuando envía el servidor al cliente
¿Qué significan esos valores?
## El tiempo de retardo entre la llegada de los paquetes

Vamos a ver ahora los valores de una trama concreta, la número 27. Vamos a analizarla en opción Telephony, RTP, RTP Stream Analysis:

¿Cuánto valen el delta y el jitter para ese paquete?
## Delta = 0.000164 y el jitter = 3.087182
¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
## Sí, observando el valor de skew
El "skew" es negativo, ¿qué quiere decir eso?
## Cuando está en negativo significa que los paquetes llegan más tarde de lo esperado

En el panel Stream Analysis puedes hacer play sobre los streams:

¿Qué se oye al pulsar play?
## Se escucha una canción
¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
## Se escucha la canción pero más lenta y con cortes
¿A qué se debe la diferencia?
## Como el Jitter Buffer es menor, los paquetes no llegan a tiempo para la reproducción

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 10. Llamadas VoIP
Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú Telephony selecciona el menú VoIP calls, y selecciona la llamada hasta que te salga el panel correspondiente:

¿Cuánto dura la llamada?
## 10 segundos
Ahora, pulsa sobre Flow Sequence:

Guarda el diagrama en un fichero (formato PNG) con el nombre diagrama.png.
## Exportado al fichero ptavi-p5
¿En qué segundo se recibe el último OK que marca el final de la llamada?
## En el segundo 10.521629838

Ahora, selecciona los dos streams, y pulsa sobre Play Sterams, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):
¿Cuáles son las SSRC que intervienen?
## Cliente = 0x0d2db8b4 y Servidor = 0x5c44a34b
¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
## Se envian 514 paquetes
¿Cuál es la frecuencia de muestreo del audio?
## 8 KHz
¿Qué formato se usa para los paquetes de audio (payload)?
## g711U
Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 11. Captura de una llamada VoIP
Desde LinPhone, créate una cuenta SIP.  A continuación:

Captura una llamada VoIP con el identificador SIP sip:music@sip.iptel.org, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero linphone-music.pcapng. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
¿Cuántos flujos RTP tiene esta captura?
¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.